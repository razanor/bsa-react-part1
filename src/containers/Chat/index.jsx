import React from 'react';
import Spinner from '../../components/Spinner';
import Logo from '../../components/Logo';
import Header from '../../components/Header';
import MessageList from '../../components/MessageList';
import MessageInput from '../../components/MessageInput';
import callApi from '../../helpers/webApiHelper';
import moment from 'moment';

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            isLoading: true
        }
        this.callApi = callApi.bind(this);
        this.updateMainState = this.handler.bind(this); 
    }

    componentDidMount() {
        this.callApi('https://api.myjson.com/bins/1hiqin');
    }

    handler(messages) {
        this.setState({
            messages
        })
    }

    render() {
        const { isLoading, messages} = this.state;
        const uniqueUsers = [...new Set(messages.map(m => m.user))].length;
        const allDates = messages.map(m => moment(m.created_at));
        const mostRecentDate = moment.max(allDates).fromNow();
        return (
            isLoading
                ? <Spinner />
                : (
                    <div className="wrapper">
                        <Logo />
                        <Header 
                            participants={uniqueUsers} 
                            messages={messages.length}
                            mostRecentDate={mostRecentDate} 
                        />
                        <MessageList 
                            messages={messages} 
                            updateMainState={this.updateMainState} 
                        />
                        <MessageInput 
                            messages={messages} 
                            updateMainState={this.updateMainState}
                        />
                    </div>
                  )
        )

    }

}

export default Chat;