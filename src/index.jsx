import React from 'react';
import { render } from 'react-dom';
import Chat from './containers/Chat/'

import 'semantic-ui-css/semantic.min.css';
import './styles/index.css';

const target = document.getElementById('root');
render(<Chat />, target);
