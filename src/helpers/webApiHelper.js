export default function callApi(url) {
    fetch(url)
        .then(res => res.json())
        .then(data => {
            this.setState({
                isLoading: false,
                messages: data
            })
        })
        .catch(err => console.error('Error:', err));
}