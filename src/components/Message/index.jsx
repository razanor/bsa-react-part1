import React from 'react';
import { Icon, Feed, Button, Modal, Form, TextArea } from 'semantic-ui-react';
import moment from 'moment';

const currentUser = 'Nazarii';

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false,
            messageBody: this.props.message.message,
            isLiked: false
        } 
    }

    hanldeDeletePost(ev) {
        const { message, messages, updateMainState } = this.props;
        const newMessages = messages.filter(m => m.id !== message.id );
        updateMainState(newMessages);
    }

    handleEditPost(ev) {
        const { message, messages, updateMainState } = this.props;
        const { messageBody } = this.state;
        if (messageBody.trim().length > 0) {
            const index = messages.findIndex(m => m.id === message.id);
            const newMessages = Object.assign([], messages,{
                [index]: {
                    id: message.id,
                    user: message.user,
                    avatar: null,
                    message: messageBody,
                    created_at: Date.now(),
                    marker_read: false
                }
            });
            updateMainState(newMessages);
            this.setState({ isModalOpen: false });
        }
    }

    render () {
        const { message } = this.props;
        const { isModalOpen, isLiked } = this.state;
        const {
            user,
            avatar,
            created_at,
            message: mes,
        } = message;

        const date = moment(created_at).fromNow();

        return (
            <div>
                <Modal 
                    closeIcon 
                    dimmer="blurring" 
                    centered={false} 
                    open={isModalOpen} 
                    onClose={ev => this.setState({isModalOpen: false})}
                >
                    <Modal.Header>Edit message</Modal.Header>
                    <Modal.Content>
                    <Form onSubmit={(ev) => this.handleEditPost(ev)}>
                        <Form.Group inline>
                            <Form.Field 
                            className="message-input" 
                            control={TextArea}  
                            placeholder='Message'
                            onChange={ev => this.setState({ messageBody: ev.target.value })}
                            value={this.state.messageBody} 
                            />
                            <Button color='vk'>Edit</Button>
                        </Form.Group>
                    </Form>
                    </Modal.Content>
                </Modal>    
                <Feed className={user === currentUser ? 'myMessage' : ''}>
                    <Feed.Event>
                        {avatar &&
                            <Feed.Label>
                                <img src={avatar} alt="avatar" />
                            </Feed.Label>
                        }
                        <Feed.Content>
                            <Feed.Summary>
                                <Feed.User>{user}</Feed.User>
                                <Feed.Date>{date}</Feed.Date>
                                { user === currentUser &&
                                    <Button 
                                        className='actionsBtn' 
                                        icon
                                        onClick={ev => this.setState({ isModalOpen: true })}
                                    >
                                        <Icon name='edit' color='green' />
                                    </Button>   
                                }
                                { user === currentUser &&
                                    <Button 
                                        className='actionsBtn' 
                                        icon 
                                        onClick={ev => this.hanldeDeletePost(ev)}
                                    >
                                        <Icon name='delete' color='red' />
                                    </Button>   
                                }
                            </Feed.Summary>
                            <Feed.Extra text>
                            { mes }
                            </Feed.Extra>
                            <Feed.Meta>
                                <Feed.Like onClick={ev => this.setState({ isLiked: !isLiked })}>
                                    <Icon name='like' />
                                    {isLiked && user !== currentUser ? 1 : 0} {' '} likes
                                </Feed.Like>
                            </Feed.Meta>
                        </Feed.Content>
                    </Feed.Event>
                </Feed>
            </div>
        );
    }
}

export default Message;