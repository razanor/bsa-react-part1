import React from 'react';
import { Segment } from 'semantic-ui-react';
import Message from '../../components/Message';


export const MessageList = ( { messages, updateMainState }) => (
    <Segment className="messagesWrapper">
        {messages.map(message => (
            <Message key={message.id} 
                message={message}
                messages={messages} 
                updateMainState={updateMainState} 
            />
        ))}
    </Segment>
);

export default MessageList;