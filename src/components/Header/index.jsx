import React from 'react';
import { Segment } from 'semantic-ui-react';

export const Header = ( { participants, messages, mostRecentDate }) => {
    return (
        <Segment className="header">
            <ul className="left-info">
                <li>My chat</li>
                <li>{participants} {' '} participants</li>
                <li>{messages} {' '} messages</li>
            </ul>
            <ul className="right-info">
                <li>Last message {' '} {mostRecentDate}</li>
            </ul>
        </Segment>
    );
};

export default Header;