import React from 'react';
import { Form, TextArea, Button, Icon } from 'semantic-ui-react';
import uuid from 'uuid';

class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messageBody: ''
        }
    }

    handleAddPost(ev) {
        const { messages, updateMainState } = this.props;
        const { messageBody } = this.state;
        if (messageBody.trim().length > 0) {
            const newPost = {
                id: uuid(),
                user: 'Nazarii',
                avatar: null,
                created_at: Date.now(),
                message: this.state.messageBody,
                marked_read: false
            }
            messages.push(newPost);
            updateMainState(messages);
            this.setState({ messageBody: '' });
        }
    }

    render() {
        return (
            <Form onSubmit={(ev) => this.handleAddPost(ev)}>
                <Form.Group inline>
                    <Form.Field 
                        className="message-input" 
                        control={TextArea}  
                        placeholder='Message'
                        onChange={ev => this.setState({ messageBody: ev.target.value })}
                        value={this.state.messageBody} 
                    />
                    <Button labelPosition='left' icon color='vk'>Send
                        <Icon name='hand point up outline' />
                    </Button>
                </Form.Group>
            </Form>
        );
    } 
}

export default MessageInput;